package ist_challenge_m_hasnul_fikri.service.impl;

import ist_challenge_m_hasnul_fikri.entity.Role;
import ist_challenge_m_hasnul_fikri.entity.User;
import ist_challenge_m_hasnul_fikri.entity.dto.RegisDTO;
import ist_challenge_m_hasnul_fikri.repository.RoleRepository;
import ist_challenge_m_hasnul_fikri.repository.UserRepository;
import ist_challenge_m_hasnul_fikri.responseHandler.ResponseHandler;
import ist_challenge_m_hasnul_fikri.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final AuthenticationManager authenticationManager;
    private final String message409 = "Username sudah dipakai";

    private final String message400 = "Username dan / atau password kosong";

    @Override
    public ResponseEntity<Object> findAllUsers() throws Exception{
        try {
            List<User> user = userRepository.findAll();
            if(user.isEmpty()){
                throw new Exception("User tidak Ditemukan");
            }
            return ResponseHandler.generateResponse("Success Retrieved User", HttpStatus.OK, user);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.NOT_FOUND,"User TIdak Ditemukan");
        }
    }

    @Override
    public ResponseEntity<Object> findById(long id) throws Exception{
       try {
           User user = userRepository.findById(id).orElseThrow(() -> new Exception("User tidak Ditemukan"));
           return ResponseHandler.generateResponse("Success Retrieved User", HttpStatus.OK, user);
       }  catch (Exception e) {
           return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.NOT_FOUND,"User tidak Ditemukan");
       }
    }
    @Override
    public ResponseEntity<Object> loginUser(RegisDTO regisDTO) throws Exception {
        try {
            if (regisDTO.getUsername() == null || regisDTO.getPassword() == null) {
                throw new Exception(message400);
            }
            Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(regisDTO.getUsername(), regisDTO.getPassword()));
            SecurityContextHolder.getContext().setAuthentication(authentication);
            User user = userRepository.findByUsername(regisDTO.getUsername()).orElseThrow(() -> new Exception("User tidak ditemukan"));
            return ResponseHandler.generateResponse("Sukses Login", HttpStatus.OK, user);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.BAD_REQUEST, "Login Failed");
        }
    }
    @Override
    public ResponseEntity<Object> registerUser(RegisDTO regisDTO) throws Exception {
        try {
            if (regisDTO.getUsername() == null || regisDTO.getPassword() == null) {
                throw new Exception(message400);
            }
            Optional<Role> roleCheck = roleRepository.findByName("ROLE_USER");
            Role role;
            if (roleCheck.isEmpty()) {
                Role newRole = new Role();
                newRole.setName("ROLE_USER");
                role = roleRepository.save(newRole);
            } else {
                role = roleCheck.get();
            }
            Optional<User> users = userRepository.findByUsername(regisDTO.getUsername());
            if (users.isPresent()) {
                throw new Exception(message409);
            }
            User user = regisDTO.saveToEntity();
            user.setRoles(Collections.singleton(role));
            User userResponse = userRepository.save(user);
            return ResponseHandler.generateResponse("Sukses Registrasi", HttpStatus.CREATED, userResponse);
        } catch (Exception e) {
            HttpStatus status;
            if (e.getMessage().equals(message409)) {
                status = HttpStatus.CONFLICT;
            } else {
                status = HttpStatus.BAD_REQUEST;
            }
            return ResponseHandler.generateResponse(e.getMessage(), status, "Login Failed");
        }
    }

    @Override
    public ResponseEntity<Object> editUser(RegisDTO regisDTO, long id) throws Exception {
        try {
            if (regisDTO.getUsername() == null || regisDTO.getPassword() == null) {
                throw new Exception(message400);
            }
            User users = userRepository.findById(id).orElseThrow(() -> new Exception("User not found"));
            Optional<User> checkUsername = userRepository.findByUsername(regisDTO.getUsername());
            if (checkUsername.isPresent()) {
                throw new Exception(message409);
            }
            if (regisDTO.getPassword().equals(users.getPassword())) {
                throw new Exception("Password tidak boleh sama dengan password sebelumnya");
            }
            users.setUsername(regisDTO.getUsername());
            users.setPassword(regisDTO.getPassword());
            User user = userRepository.save(users);
            return ResponseHandler.generateResponse("Sukses Edit", HttpStatus.CREATED, user);
        } catch (Exception e) {
            HttpStatus status;
            if (e.getMessage().equals(message409)) {
                status = HttpStatus.CONFLICT;
            } else {
                status = HttpStatus.BAD_REQUEST;
            }
            return ResponseHandler.generateResponse(e.getMessage(), status, "Login Failed");
        }

    }

}
