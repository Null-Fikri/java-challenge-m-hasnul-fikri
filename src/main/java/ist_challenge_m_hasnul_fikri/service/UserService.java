package ist_challenge_m_hasnul_fikri.service;

import ist_challenge_m_hasnul_fikri.entity.dto.RegisDTO;
import org.springframework.http.ResponseEntity;

public interface UserService {

    ResponseEntity<Object> findAllUsers()throws Exception;

    ResponseEntity<Object> registerUser(RegisDTO regisDTO) throws Exception;


    ResponseEntity<Object> editUser(RegisDTO regisDTO, long id) throws Exception;

    ResponseEntity<Object> findById(long id) throws Exception;

    ResponseEntity<Object> loginUser(RegisDTO regisDTO) throws Exception;

}
