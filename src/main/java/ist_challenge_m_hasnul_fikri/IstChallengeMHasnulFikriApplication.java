package ist_challenge_m_hasnul_fikri;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication
public class IstChallengeMHasnulFikriApplication {

	public static void main(String[] args) {
		SpringApplication.run(IstChallengeMHasnulFikriApplication.class, args);
	}
	@Bean
	PasswordEncoder passwordEncoder() {
		return NoOpPasswordEncoder.getInstance();
	}
}
