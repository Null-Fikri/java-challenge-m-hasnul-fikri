package ist_challenge_m_hasnul_fikri.entity.dto;

import ist_challenge_m_hasnul_fikri.entity.User;
import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RegisDTO {

    private String username;

    private String password;

    public User saveToEntity(){
        return User.builder()
                .username(this.username)
                .password(this.password)
                .build();
    }
}
