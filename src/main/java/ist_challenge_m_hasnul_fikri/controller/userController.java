package ist_challenge_m_hasnul_fikri.controller;

import ist_challenge_m_hasnul_fikri.entity.dto.RegisDTO;
import ist_challenge_m_hasnul_fikri.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@AllArgsConstructor
public class userController {

    private final UserService userService;

    @PostMapping("/register")
    public ResponseEntity<Object> registerUser(@RequestBody RegisDTO regisDTO) throws Exception {
        return userService.registerUser(regisDTO);
    }

    @PostMapping("/login")
    public ResponseEntity<Object> authenticateUser(@Valid @RequestBody RegisDTO regisDTO) throws Exception {
        return userService.loginUser(regisDTO);
    }

    @GetMapping("/users")
    public ResponseEntity<Object> getUsers() throws Exception {
        return userService.findAllUsers();
    }
    @GetMapping("/user/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable long id) throws Exception {
        return userService.findById(id);
    }

    @PutMapping("/user/{id}")
    public ResponseEntity<Object> editUsers(@PathVariable long id, @RequestBody RegisDTO regisDTO) throws Exception {
        return userService.editUser(regisDTO, id);
    }
}
