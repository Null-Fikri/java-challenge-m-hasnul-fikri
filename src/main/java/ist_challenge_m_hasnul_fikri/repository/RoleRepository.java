package ist_challenge_m_hasnul_fikri.repository;

import ist_challenge_m_hasnul_fikri.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(String role);
}
