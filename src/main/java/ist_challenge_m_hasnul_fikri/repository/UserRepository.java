package ist_challenge_m_hasnul_fikri.repository;

import ist_challenge_m_hasnul_fikri.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByUsername(String keyword);
}
